from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='project_to_test_1_2022-02-08_v1_dev',
    packages=find_packages(where='src', exclude=['tests']),
    package_dir={'': 'src'},
    version=version,
    description='project to test 1',
    author='po_1 po_1'
)
