from datetime import timedelta

from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.providers.docker.operators.docker import DockerOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['po_1 po_1'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    dag_id='project_to_test_1_2022-02-08_v1_dev',
    default_args=default_args,
    schedule_interval=None,
    is_paused_upon_creation=False
)

scoring = DockerOperator(
    task_id='scoring',
    mount_tmp_dir=False,
    dag=dag,
    image='msk-hdp-dn171.megafon.ru:5000/megafon/mlflow_project_runner:0.1.20',
    command=["-m", "project_to_test_1_2022-02-08_v1_dev", "-v", "$VERSION", "-c", "score", "-P",
             "snap_date={{dag_run.conf['snap_date']}}"],
    environment={
        'MLFLOW_S3_ENDPOINT_URL': '{{var.value.MLFLOW_S3_ENDPOINT_URL}}',
        'AWS_ACCESS_KEY_ID': '{{var.value.MLFLOW_ACCESS_KEY_ID}}',
        'AWS_SECRET_ACCESS_KEY': '{{var.value.MLFLOW_ACCESS_KEY_SECRET}}',
    }
)

export = DockerOperator(
    task_id='export',
    mount_tmp_dir=False,
    dag=dag,
    image='msk-hdp-dn171.megafon.ru:5000/megafon/mlflow_project_runner:0.1.20',
    command=["-m", "temp_1", "-v", "$VERSION", "-c", "export_oracle",
             "-P", "snap_date={{dag_run.conf['snap_date']}}",
             "-P", "username={{var.value.UAT_DATABASE_USERNAME}}",
             "-P", "password={{var.value.UAT_DATABASE_PASSWORD_SECRET}}",
             "-P", "jdbc={{var.value.UAT_JDBC_CONNECTION_STRING}}",
             ]
)

scoring >> export
